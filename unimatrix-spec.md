# UniMatrix Specification

The [UniMatrix Project][unimatrix] develops specifications to allow management of cloud native applications in the physical security industry ecosystem for analytics in order to enhance interoperability.

## Abstract

...

## Table of Contents

* [Notational Conventions](#notational_conventions)
* [Host Requirements](#host_requirements)
* [Base Layers](#base_layers)
  * [Base](#base)
  * [Base-python](#base-python)
* [Video API](#video_api)
* [Inference API](#inference_api)
* [System Containers](#system_containers)

## <a name="notational_conventions"></a>Notational Conventions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" are to be interpreted as described in [RFC 2119][rfc2119].

The key words "unspecified", "undefined", and "implementation-defined" are to be interpreted as described in the [rationale for the C99 standard][c99-unspecified].

An implementation is not compliant if it fails to satisfy one or more of the MUST, REQUIRED, or SHALL requirements.
An implementation is compliant if it satisfies all the MUST, REQUIRED, and SHALL requirements.

## <a name="host_requirements"></a>Host Requirements

1. The shell script ```check-config.sh```, located in the same repository as this specification, verifies that the mandatory Linux kernel config options are enabled. It SHALL be possible to run this script without errors on a compliant host. Warnings about disabled optional config options are allowed but SHOULD be observed as they may indicate a non optimal runtime environment.

1. The host SHALL be able to create and start containers from an [OCI bundle][oci-bundle] using a CLI command, e.g. [runc][runc], that is compatible with the [CLI spec][oci-cli]. It shall be possible to check state of, kill and delete running containers. This SHALL be verified by running a runc test suite without errors on the host.

1. The host SHALL support the [containerd][containerd] runtime including the [containerd gRPC API][containerd-grpc] for remotely managing containers on the host. This SHALL be verified by running a containerd test suite without errors on the host.

1. The host SHALL have a D-Bus service running with a bus that is accessible from within a container. This SHALL be verified by running a D-Bus test suite on the host.

1. The host SHALL provide the OpenCV videoio C/C++ dynamic library with a Video I/O backend that is compatible with the host hardware. For example, if the host supports Video4Linux, the videoio library SHALL support the V4L backend. If the host supports retrieving media of RTSP/RTP it shall support the ffmpeg backend. Further requirements for the supported backend is described in the [Video API](#video_api) section.

## <a name="base_layers"></a>Base Layers

The UniMatrix APIs are provided to applications by a set of base container layers that applications can inherit from, e.g. by using a ```Dockerfile``` for building the application.

```Dockerfile
FROM registry.gitlab.com/unimatrix/containers/arm32v7/base:latest
...
```

1. There SHALL be two levels of container layers, base and base-python, where everything in base is also in base-python. These are the deployment layers that gets deployed to the host together with the application.

1. The following CPU architectures SHALL be supported. For every UniMatrix release the container layers SHALL be built for these architectures.
    1. arm32v7/armhf
    1. arm64v8/aarch64
    1. amd64

1. For each container layer there SHALL be a corresponding development layer that contains files needed to develop the application on a development host. This SHOULD include things like:
    1. Header files
    1. Debug symbols
    1. Examples
    1. Documentation

### <a name="base"></a>Base

1. Base SHALL inherit the official [Ubuntu image][ubuntu-hub]. The exact version of Ubuntu SHALL be documented for every UniMatrix release.

1. Base SHALL include C/C++ language bindings for the following APIs:
    1. Video API
    1. Inference API

### <a name="base-python"></a>Base-python

1. Base-python SHALL include everything that is in base.

1. Base-python SHALL include Python 3 support, including interpreter, package manager (pip) and common Python modules that are dependencies of the UniMatrix APIs, including:
    1. NumPy
    1. protobuf
    1. grpcio

1. Base-python SHALL include Python 3 language bindings for the following APIs:
    1. Video API

1. Base-python SHALL include an Inference gRPC API.

## <a name="video_api"></a>Video API

The Video API is based on [OpenCV][opencv].

1. OpenCV core SHALL be supported.

## <a name="inference_api"></a>Inference API

...

## <a name="system_containers"></a>System Containers

...

[unimatrix]: https://unimatrix.ai/
[rfc2119]: http://tools.ietf.org/html/rfc2119
[c99-unspecified]: http://www.open-std.org/jtc1/sc22/wg14/www/C99RationaleV5.10.pdf#page=18
[oci-bundle]: https://github.com/opencontainers/runtime-spec/blob/master/bundle.md
[oci-cli]: https://github.com/opencontainers/runtime-tools/blob/master/docs/command-line-interface.md
[runc]: https://github.com/opencontainers/runc
[containerd]: https://github.com/containerd/containerd
[containerd-grpc]: https://dnephin.github.io/containerd/github.com/containerd/containerd/api/services/containers/v1/containers.html
[ubuntu-hub]: https://hub.docker.com/_/ubuntu
[opencv]: https://opencv.org/
